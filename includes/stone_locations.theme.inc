<?php

/**
 * Preprocess variables for stone_locations.contact.tpl.php.
 */
function template_preprocess_stone_locations_contact(&$variables) {
  if ($location = $variables['location']) {
    $variables['name'] = '';

    // Only set location name if we're not on the location page.
    if (current_path() != 'node/' . $location->nid) {
      $variables['name'] = l($location->title, 'node/' . $location->nid);
    }

    // Set address, telephone and fax fields.
    foreach (array('address', 'telephone', 'fax', 'email') as $field_name) {
      $variables[$field_name] = NULL;

      if ($field = field_get_items('node', $location, 'field_location_' . $field_name)) {
        $variables[$field_name] = field_view_value('node', $location, 'field_location_' . $field_name, $field[0]);
      }
    }

    // Set social media fields.
    foreach (array('facebook', 'twitter') as $field_name) {
      if ($field = field_get_items('node', $location, 'field_location_social_' . $field_name)) {
        $variables['social_links'][$field_name] = l(
          t("View @location's @field profile", array(
            '@location' => $location->title,
            '@field' => $field_name,
          )),
          drupal_render(field_view_value('node', $location, 'field_location_social_' . $field_name, $field[0])),
          array(
            'attributes' => array(
              'class' => array($field_name),
            ),
          )
        );
      }
    }

    $variables['more_details'] = '';

    // Only set more_details button if we're not on the location page.
    if (current_path() != 'node/' . $location->nid) {
      $variables['more_details'] = t('<a href="@path" class="Button">More details</a>', array('@path' => url('node/' . $location->nid)));
    }
  }
}

/**
 * Preprocess function for the stone_locations.list-*-group templates.
 */
function template_preprocess_stone_locations_list_group(&$variables) {
  $variables['group_title_html_safe'] = drupal_html_id($variables['group_title']);

  // Build an array of names for the locations in this group.
  $variables['names'] = array();

  foreach ($variables['locations'] as $location) {
    $variables['names'][$location->nid] = $location->title;
  }
}

/**
 * Preprocess function for the stone_locations.list-location-group template.
 */
function template_preprocess_stone_locations_list_location_group(&$variables) {
  // Further group locations by locality.
  $variables['localities'] = array();

  foreach ($variables['locations'] as $location) {
    $address = field_get_items('node', $location, 'field_location_address');

    $locality_title = $address[0]['locality'];
    $variables['localities'][$locality_title][] = $location;

    if (!isset($variables['locality_ids'][$locality_title])) {
      $variables['locality_ids'][$locality_title] = drupal_html_id($locality_title);
    }
  }
}

/**
 * Preprocess function for the stone_locations.list-letter-group template.
 */
function template_preprocess_stone_locations_list_letter_group(&$variables) {
  // Add the locality part of the address to each locations name.
  foreach ($variables['locations'] as $location) {
    $address = field_get_items('node', $location, 'field_location_address');

    if (!empty($address[0]['locality'])) {
      $variables['names'][$location->nid] .= ', ' . $address[0]['locality'];
    }
  }
}
