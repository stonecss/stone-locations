<?php
/**
 * @file
 * stone_locations.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function stone_locations_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:stone_locations_main';
  $panelizer->title = 'Locations: Main';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '1_column';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
      'primary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'dcdc12de-71c1-4249-943b-7012cc05ddd6';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-c42b30b9-0fdd-48cc-ba68-11cf87bb33be';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c42b30b9-0fdd-48cc-ba68-11cf87bb33be';
    $display->content['new-c42b30b9-0fdd-48cc-ba68-11cf87bb33be'] = $pane;
    $display->panels['primary'][0] = 'new-c42b30b9-0fdd-48cc-ba68-11cf87bb33be';
    $pane = new stdClass();
    $pane->pid = 'new-9cfac69f-a7bb-4740-8885-2e3888add8d9';
    $pane->panel = 'primary';
    $pane->type = 'block';
    $pane->subtype = 'stone_locations-stone_locations_search_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '9cfac69f-a7bb-4740-8885-2e3888add8d9';
    $display->content['new-9cfac69f-a7bb-4740-8885-2e3888add8d9'] = $pane;
    $display->panels['primary'][1] = 'new-9cfac69f-a7bb-4740-8885-2e3888add8d9';
    $pane = new stdClass();
    $pane->pid = 'new-17a642c1-67f4-4eaf-a998-4ae94539c153';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '17a642c1-67f4-4eaf-a998-4ae94539c153';
    $display->content['new-17a642c1-67f4-4eaf-a998-4ae94539c153'] = $pane;
    $display->panels['primary'][2] = 'new-17a642c1-67f4-4eaf-a998-4ae94539c153';
    $pane = new stdClass();
    $pane->pid = 'new-1cc3373f-e638-4846-9eeb-66eb79c2d2e1';
    $pane->panel = 'primary';
    $pane->type = 'block';
    $pane->subtype = 'stone_locations-stone_locations_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '1cc3373f-e638-4846-9eeb-66eb79c2d2e1';
    $display->content['new-1cc3373f-e638-4846-9eeb-66eb79c2d2e1'] = $pane;
    $display->panels['primary'][3] = 'new-1cc3373f-e638-4846-9eeb-66eb79c2d2e1';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:stone_locations_main'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:stone_locations_search';
  $panelizer->title = 'Locations: Search';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = '1_column';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
      'primary' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '6d4e4ffe-2dab-47d8-81ac-147544ce532d';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-3562a424-19d7-44ae-b987-11801659f8ad';
    $pane->panel = 'primary';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3562a424-19d7-44ae-b987-11801659f8ad';
    $display->content['new-3562a424-19d7-44ae-b987-11801659f8ad'] = $pane;
    $display->panels['primary'][0] = 'new-3562a424-19d7-44ae-b987-11801659f8ad';
    $pane = new stdClass();
    $pane->pid = 'new-fe84760e-aa9c-4a70-bc53-fe6bbe808157';
    $pane->panel = 'primary';
    $pane->type = 'block';
    $pane->subtype = 'stone_locations-stone_locations_search_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'fe84760e-aa9c-4a70-bc53-fe6bbe808157';
    $display->content['new-fe84760e-aa9c-4a70-bc53-fe6bbe808157'] = $pane;
    $display->panels['primary'][1] = 'new-fe84760e-aa9c-4a70-bc53-fe6bbe808157';
    $pane = new stdClass();
    $pane->pid = 'new-434129db-63f1-4672-ac56-9be3ef38eb0c';
    $pane->panel = 'primary';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '434129db-63f1-4672-ac56-9be3ef38eb0c';
    $display->content['new-434129db-63f1-4672-ac56-9be3ef38eb0c'] = $pane;
    $display->panels['primary'][2] = 'new-434129db-63f1-4672-ac56-9be3ef38eb0c';
    $pane = new stdClass();
    $pane->pid = 'new-ce461769-07fa-4085-a66f-cb8d613fd674';
    $pane->panel = 'primary';
    $pane->type = 'block';
    $pane->subtype = 'stone_locations-stone_locations_search_results_map';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'ce461769-07fa-4085-a66f-cb8d613fd674';
    $display->content['new-ce461769-07fa-4085-a66f-cb8d613fd674'] = $pane;
    $display->panels['primary'][3] = 'new-ce461769-07fa-4085-a66f-cb8d613fd674';
    $pane = new stdClass();
    $pane->pid = 'new-975e31e9-e080-43ed-8e67-32605dc3b649';
    $pane->panel = 'primary';
    $pane->type = 'block';
    $pane->subtype = 'stone_locations-stone_locations_search_results_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '975e31e9-e080-43ed-8e67-32605dc3b649';
    $display->content['new-975e31e9-e080-43ed-8e67-32605dc3b649'] = $pane;
    $display->panels['primary'][4] = 'new-975e31e9-e080-43ed-8e67-32605dc3b649';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:stone_locations_search'] = $panelizer;

  return $export;
}
