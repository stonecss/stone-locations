<div class="Location-contactDetails">
  <?php if ($name): ?>
    <h3 class="Location-name"><?php print $name; ?></h3>
  <?php endif; ?>

  <?php if ($address): ?>
    <div class="Location-address">
      <h4>Address</h4>
      <?php print render($address); ?>
    </div>
  <?php endif; ?>

  <?php if ($telephone || $fax || $email): ?>
    <div class="Location-contact">
      <h4>Contact</h4>
      <?php if ($telephone): ?>
        <div class="Location-telephone"><strong>T:</strong> <?php print render($telephone); ?></div>
      <?php endif; ?>
      <?php if ($fax): ?>
        <div class="Location-telephone"><strong>F:</strong> <?php print render($fax); ?></div>
      <?php endif; ?>
      <?php if ($email): ?>
        <div class="Location-email"><strong>E:</strong> <?php print render($email); ?></div>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($social_links)): ?>
    <ul class="Location-socialLinks">
      <?php foreach ($social_links as $social_link): ?>
        <?php if ($social_link): ?>
          <li class="Location-socialLink"><?php print render($social_link); ?></li>
        <?php endif; ?>
      <?php endforeach; ?>
    </ul>
  <?php endif; ?>

  <?php if ($more_details): ?>
    <?php print render($more_details); ?>
  <?php endif; ?>
</div>
