<div class="Box Box--locationsListGroup Grid-cell" id="county-<?php echo $group_title_html_safe ?>">
  <div class="Box-inner">
    <h2 class="Box-title">
      <?php echo $group_title ?>
    </h2>

    <div class="Box-content">
      <?php foreach ($localities as $title => $locations): ?>
        <div class="Box Box--locationsListLocality" id="locality-<?php echo $locality_ids[$title] ?>">
          <h3 class="Box-title">
            <?php echo $title ?>
          </h3>

          <ul class="Box-content">
            <?php foreach ($locations as $location): ?>
              <li><?php echo l($names[$location->nid], 'node/' . $location->nid) ?></li>
            <?php endforeach; ?>
          </ul>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>
