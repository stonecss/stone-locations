<div class="LocationSearchResults-listItem">
  <div class="LocationSearchResults-title">
    <?php echo l($location->title, 'node/' . $location->nid) ?>
  </div>

  <?php if (isset($distance)): ?>
  <div class="LocationSearchResults-distance">
    <?php echo $distance ?>
  </div>
  <?php endif; ?>
</div>
