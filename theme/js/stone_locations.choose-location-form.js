(function($) {

Drupal.behaviors.stone_locations_choose_form = {
  attach: function (context, settings) {
    // Make the 'choose location' form more user friendly.
    $('.stone-locations-choose-location-form').each(function() {
      // Submit the form when the selection is changed.
      $(this).find('select').change(function() {
        $(this).parents('form').submit();
      });

      // The submit button is no longer required...
      $(this).find('input[type=submit]').hide();
    });
  }
};

})(jQuery);
