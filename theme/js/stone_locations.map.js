(function($) {

Drupal.behaviors.stone_locations_map = {
  attach: function (context, settings) {
    if (typeof Drupal.settings.stone_locations.map === 'undefined') {
      return;
    }

    var options = Drupal.settings.stone_locations.map;

    // To store all the markers for future JS use.
    Drupal.settings.stone_locations.markers = [];

    $('.LocationMap').each(function(index, element) {
      // Build options to pass to the maps API.
      var map_options = {
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoom: options.zoom || 12,
        mapTypeControl: false,
        center: new google.maps.LatLng(
          options.center.geo.latitude,
          options.center.geo.longitude
        )
      };

      if (typeof options.additional_options !== 'undefined') {
        jQuery.extend(map_options, options.additional_options);
      }

      var map = new google.maps.Map(element, map_options),
        info_window = new google.maps.InfoWindow(),

      // Function used to set the content and position of the above info window
      // on the above map when a user clicks a marker.
      placeInfoWindow = function(marker, content) {
        google.maps.event.addListener(marker, 'click', function() {
          info_window.setContent(content);
          info_window.open(map, marker);
        });
      };

      var bounds = new google.maps.LatLngBounds();

      if (typeof options.showCenterMarker === 'undefined') {
        // Add a marker at the center.
        marker_options = {
          map: map,
          position: map_options.center
        };

        if (typeof options.marker_images !== 'undefined') {
          if (typeof options.marker_images.center !== 'undefined') {
            marker_options.icon = options.marker_images.center;
          }
        }

        var center_marker = new google.maps.Marker(marker_options);

        // Info window at the center?
        if (typeof options.center.info_window !== 'undefined') {
          placeInfoWindow(center_marker, options.center.info_window);
        }

        // Extend the bounds
        bounds.extend(map_options.center);
      }

      // If we've been given a set of markers then place these on the map too.
      if (typeof options.markers !== 'undefined') {
        for (var i in options.markers) {
          // Build options for this marker.
          var lat_lon = new google.maps.LatLng(
            options.markers[i].geo.latitude,
            options.markers[i].geo.longitude
          ),
          location_marker_options = {
            map: map,
            position: lat_lon
          }

          if (typeof options.marker_images !== 'undefined') {
            if (typeof options.marker_images.location !== 'undefined') {
              location_marker_options.icon = options.marker_images.location;
            }
          }

          // Add additional marker attributes/options
          if (typeof options.markers[i].additional_options !== 'undefined') {
            $.extend(location_marker_options, options.markers[i].additional_options);
          }

          var location_marker = new google.maps.Marker(location_marker_options);

          // Does the marker define content for an info window?
          if (typeof options.markers[i].info_window !== 'undefined') {
            placeInfoWindow(location_marker, options.markers[i].info_window);
          }

          // Add marker to globally accessible array.
          Drupal.settings.stone_locations.markers.push(location_marker);

          // Extend the bounds of the map to accommodate this marker.
          bounds.extend(lat_lon);
        }

        map.fitBounds(bounds);
      }

      // Store the map object and options somewhere that other JS can access it.
      Drupal.settings.stone_locations.map.objMap = map;
      Drupal.settings.stone_locations.map.options = map_options;
    });
  }
};

})(jQuery);
